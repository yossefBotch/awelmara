#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "user.h"
void STK_create(tstack* ps)
{
    ps->top=0;
}

void STK_Push(tstack*ps,STACK_DATATYPE e)
{
    ps->arr[ps->top++]=e;
}

void STK_Pop(tstack* ps,STACK_DATATYPE*pe)
{
    ps->top--;
    *pe=ps->arr[ps->top];
}
int STK_Full(tstack*ps)
{
    if((ps->top)==MAXSTACK)
    {
        return 1;
    }
else return 0;
}

int STK_Empty(tstack*ps)
{
    if((ps->top)==0)
    {
        return 1;
    }
    else return 0;
}

void STK_Top(tstack*ps,STACK_DATATYPE*pe)
{
*pe=ps->arr[ps->top++];
}

int STK_Size(tstack*ps)
{
return ps->top;
}
void STK_clear(tstack*ps)
{
     ps->top=0;
}
