#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED
#include"user.h"
typedef struct
{
    STACK_DATATYPE arr[MAXSTACK];
    unsigned short top;
}tstack;
void STK_create(tstack* ps);
void STK_Push(tstack*ps,STACK_DATATYPE e);
void STK_Pop(tstack* ps,STACK_DATATYPE*pe);
int STK_Full(tstack*ps);
int STK_Empty(tstack*ps);
void STK_Top(tstack*ps,STACK_DATATYPE*pe);
int STK_Size(tstack*ps);
void STK_clear(tstack*ps);


#endif // STACK_H_INCLUDED
